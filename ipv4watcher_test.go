package ipwatch_test

import (
	"context"
	"testing"

	"bitbucket.org/orangeparis/ipwatch"
	nats "github.com/nats-io/nats.go"

	//"github.com/stretchr/testify/assert"

	//"fmt"

	"time"
)

func TestIpv4WatcherRun(t *testing.T) {

	nc, err := nats.Connect("nats://demo.nats.io:4222")
	if err != nil {
		t.Fail()
		return
	}
	defer nc.Close()
	//var cidr ipwatch.Cidr = "192.168.1.0/24"
	cidr := "192.168.1.0/24"
	w, _ := ipwatch.NewIpv4Watcher(nc, "1", cidr, 80, 500, 0)
	//w := ipwatch.Ipv4Watcher{nc, "1", cidr, []int{80}, 500 * time.Millisecond, 0}
	ctx, cancel := context.WithCancel(context.Background())

	// start the watcher
	go w.Run(ctx)

	time.Sleep(3 * time.Second)
	cancel()
	time.Sleep(1 * time.Second)

}

// func TestIpv4Watcher(t *testing.T) {

// 	nc, err := nats.Connect("nats://demo.nats.io:4222")
// 	if err != nil {
// 		t.Fail()
// 		return
// 	}
// 	defer nc.Close()
// 	var cidr ipwatch.Cidr = "192.168.1.0/24"
// 	w := ipwatch.Ipv4Watcher{nc, "1", cidr, []int{80, 23}, 500 * time.Millisecond, 90}
// 	w.ScanOpenPorts()

// }
