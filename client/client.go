package client

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"bitbucket.org/orangeparis/ipwatch/service"
	nats "github.com/nats-io/nats.go"
)

/*
	a client for nats service ipwatch
*/

// Client a client for ipwatch nats service
type Client struct {
	*nats.Conn
	Timeout time.Duration
}

type Request struct {
	Params interface{} `json:"params"` // watcher name
}

// NewClient create a nats client to ipwatch service
func NewClient(natsURL string, timeout int) (client *Client, err error) {

	// create a ipv4 watcher controller
	nc, err := nats.Connect(natsURL)
	if err != nil {
		log.Printf("ipwatch.client: cannot connect to %s\n", natsURL)
		return client, err
	}
	if timeout == 0 {
		timeout = 5
	}

	client = &Client{nc, time.Duration(time.Duration(timeout) * time.Second)}

	return client, err
}

// StartIpv4  an ipv4 watcher  call.ipwatch.ipv4.start
func (c *Client) StartIpv4(name string, cidr string, port int, timeout int, logLevel int) (m *nats.Msg, err error) {

	subject := "call.ipwatch.ipv4.start"
	in := &service.NewIn{Name: name, Cidr: cidr, Port: port, Timeout: timeout}
	req := &Request{Params: in}
	msg, err := json.Marshal(req)
	if err != nil {
		return m, err
	}
	m, err = c.Request(subject, msg, c.Timeout)
	if err != nil {
		log.Printf("ipwatch client: request error: %s", err.Error())
		return m, err
	}
	//println(string(m.Data))
	return m, err
}

// ListIpv4 list active ipv4 watchers get.ipwatch.ipv4
func (c *Client) ListIpv4() (m *nats.Msg, err error) {
	subject := "get.ipwatch.ipv4"
	m, err = c.Request(subject, []byte("null"), c.Timeout)
	if err != nil {
		log.Printf("error: %s", err)
	}
	return m, err
}

// StopIpv4 kill an ipv4 watch call.ipwatch.ipv4.$id.stop
func (c *Client) StopIpv4(id string) (m *nats.Msg, err error) {

	subject := fmt.Sprintf("call.ipwatch.ipv4.%s.stop", id)
	m, err = c.Request(subject, []byte("null"), c.Timeout)
	if err != nil {
		log.Printf("error: %s", err)
	}
	return m, err
}

// GetIpv4 get model of an ipv4 watcher : get.ipwatch.ipv4.$id
func (c *Client) GetIpv4(id string) (m *nats.Msg, err error) {

	subject := fmt.Sprintf("get.ipwatch.ipv4.%s", id)
	m, err = c.Request(subject, []byte("null"), c.Timeout)
	if err != nil {
		log.Printf("error: %s", err)
	}
	return m, err
}

// ClearAllIpv4 kill all ipv4 watchers call.ipwatch.ipv4.clear
func (c *Client) ClearAllIpv4() (m *nats.Msg, err error) {
	subject := "call.ipwatch.ipv4.clear"
	m, err = c.Request(subject, []byte("null"), c.Timeout)
	if err != nil {
		log.Printf("error: %s", err)
	}
	return m, err
}

// GetIpv4Hits get hits of an ipv4 watcher : get.ipwatch.ipv4.$id.hits
func (c *Client) GetIpv4Hits(id string) (m *nats.Msg, err error) {

	subject := fmt.Sprintf("get.ipwatch.ipv4.%s.hits", id)
	m, err = c.Request(subject, []byte("null"), c.Timeout)
	if err != nil {
		log.Printf("error: %s", err)
	}
	return m, err
}
