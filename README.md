# IPWATCH #

a nats service "ipwatch" to scan ip within a cidr and find open ports



# ipwatch.ipv4  

an ipv4 scanner to find open ports

## resources and methods

## call.ipwatch.ipv4.start

in:

	cidr string  // eg 192.168.1.0/24 
	ports []int  // list of ports to scan
	workers int  // number of workers
	
	
out:

	rid string : resource id  eg ipwatch.ipv4.2
	
## get.ipwatch.ipv4

get a list of ipv4 watchers

out:
	[rid1,rid2]  eg ["ipwatch.ipv4.2",ipwatch.ipv4.3"]
	
	
	
## get.ipwatch.ipv4.$id

get representation of an ipv4 watcher

	cidr string 
	ports [] int
	workers int
	uptime int  // ( in seconds )


## call.ipwatch.ipv4.$id.stop

shutdown an ipwatcher



# events produced by ip watchers

ip watchers like ipwatch.ipv4.5 produce events as they found open ports

subject:

	event.ipwatch.ipv4.$id.found
	
message:
	
	ip string
	port int
	
	
