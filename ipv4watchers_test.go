package ipwatch_test

import (
	"testing"

	"bitbucket.org/orangeparis/ipwatch"
	nats "github.com/nats-io/nats.go"

	//"github.com/stretchr/testify/assert"

	//"fmt"

	"time"
)

func TestIpv4WatcherController(t *testing.T) {

	nc, err := nats.Connect("nats://demo.nats.io:4222")
	if err != nil {
		t.Fail()
		return
	}
	defer nc.Close()

	// create a controller
	c := ipwatch.NewIpv4Watchers(nc, "ipv4")

	// set a config for an ipwatcher
	//var cidr ipwatch.Cidr = "192.168.1.0/24"
	//cnf := &ipwatch.Ipv4WatcherParams{"sample", cidr, []int{80}, 500 * time.Millisecond, 90}

	//cnf := ipwatch.NewIpv4WatchersParams("sample", "192.168.1.0/24", 80, 500, 90)

	l1 := c.MemberList()
	_ = l1

	//ctx, cancel := context.WithCancel(context.Background())
	c.New("sample", "192.168.1.0/24", 80, 500, 90)

	l2 := c.MemberList()
	_ = l2

	time.Sleep(2 * time.Second)

	// get the hits
	hits, _ := c.GetHits("sample")
	print(hits)

	time.Sleep(5 * time.Second)

	c.ClearAll()
	//c.Clear("sample")

	// stop the watcher
	//c.Members["sample"].Cancel()
	// cancel()
	time.Sleep(1 * time.Second)

}
