package tests

import (
	"encoding/json"
	//"log"
	"testing"
	"time"

	"bitbucket.org/orangeparis/ipwatch/service"
)

var (
	natsServer = "nats://demo.nats.io:4222"
)

/*

	a sample usage of resgate service ipwatch.ipv4



*/

//
func TestIpwatchAsAResgateClient(t *testing.T) {

	// start the ipwatch server
	ipwatchService := service.NewService(natsServer)
	defer ipwatchService.Shutdown()

	time.Sleep(2 * time.Second)

	// create a resgate nats client
	client, err := NewRsClient(natsServer, 3, true, true)
	if err != nil {
		t.Fail()
		return
	}
	defer client.Close()

	//
	// begin usage
	//

	// start an ipv4 watcher:
	//
	//  call.ipwatch.ipv4.start { Name:"http", Cidr:"192.168.1.0", Port:80, Timeout:500 }
	subject := "call.ipwatch.ipv4.start"
	args := map[string]interface{}{"name": "http", "cidr": "192.168.1.0/24", "port": 80, "timeout": 500}
	payload, err := json.Marshal(args)
	response, err := client.Request(subject, payload)
	if err != nil {
		t.Fail()
		return
	}
	println(string(response))

	time.Sleep(2 * time.Second)

	// get map of open ports
	// call.ipwatch.ipv4.http.hits -> map[string]int64
	subject = "get.ipwatch.ipv4.http.hits"
	response, err = client.Request(subject, nil)
	if err != nil {
		t.Fail()
		return
	}
	println(string(response))

	time.Sleep(2 * time.Second)

	// stop the ipv4 watcher
	//  call.ipwatch.ipv4.http.stop
	subject = "call.ipwatch.ipv4.http.stop"
	response, err = client.Request(subject, nil)
	if err != nil {
		t.Fail()
		return
	}
	println(string(response))

	time.Sleep(2 * time.Second)

}

func TestIpwatchAsAResgateClient2(t *testing.T) {

	// start the ipwatch server
	ipwatchService := service.NewService(natsServer)
	defer ipwatchService.Shutdown()

	time.Sleep(2 * time.Second)

	// create a resgate nats client
	client, err := NewRsClient(natsServer, 3, true, true)
	if err != nil {
		t.Fail()
		return
	}
	defer client.Close()

	//
	// begin usage
	//

	// start an ipv4 watcher:
	//
	//  call.ipwatch.ipv4.start { Name:"http", Cidr:"192.168.1.0", Port:80, Timeout:500 }
	subject := "call.ipwatch.ipv4.start"
	msg := struct {
		Name    string `json:"name"`
		Cidr    string `json:"cidr"`
		Port    int    `json:"port"`
		Timeout int    `json:"timeout"`
	}{"http", "192.168.1.0/24", 80, 500}

	response, err := client.Request2(subject, msg)
	if err != nil {
		t.Fail()
		return
	}
	println(string(response))

	time.Sleep(2 * time.Second)

	// get map of open ports
	// call.ipwatch.ipv4.http.hits -> map[string]int64
	subject = "get.ipwatch.ipv4.http.hits"
	response, err = client.Request2(subject, nil)
	if err != nil {
		t.Fail()
		return
	}
	println(string(response))

	time.Sleep(2 * time.Second)

	// stop the ipv4 watcher
	//  call.ipwatch.ipv4.http.stop
	subject = "call.ipwatch.ipv4.http.stop"
	response, err = client.Request2(subject, nil)
	if err != nil {
		t.Fail()
		return
	}
	println(string(response))

	time.Sleep(2 * time.Second)

}
