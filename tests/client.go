package tests

import (
	"encoding/json"
	"fmt"
	"github.com/resgateio/resgate/logger"
	client "github.com/resgateio/resgate/nats"
	"log"
	"sync"
	"time"
)

/*

	resgate client



*/

type RsClient struct {
	*client.Client        // resgate io base client
	nats           string // nats server eg nats://demo.nats.io:4222
}

type Request struct {
	Params interface{} `json:"params"` // watcher name
}

// NewRsClient create a new Resgate Client
// timeout: request default timeout in second
func NewRsClient(natsURL string, timeout int64, debug, trace bool) (rscli *RsClient, err error) {

	// create a resgate nats client
	c := &client.Client{
		RequestTimeout: time.Duration(timeout) * time.Second,
		URL:            natsURL,
		Logger:         logger.NewStdLogger(debug, trace),
	}
	rscli = &RsClient{c, natsURL}

	err = rscli.Connect()
	if err != nil {
		log.Printf("RsClient: cannot connect : %s", err.Error())
		return rscli, err
	}
	return rscli, err
}

// Request : synchrony request
// 	subject string eg call.ipwatch.ipv4.start
func (c *RsClient) Request(subject string, payload []byte) (response []byte, err error) {

	var wg sync.WaitGroup

	if payload == nil {
		payload = []byte("null")
	}
	request := fmt.Sprintf(`{"params":%s}`, string(payload))
	payload = []byte(request)

	// send a request
	wg.Add(1)
	c.SendRequest(subject,
		payload,
		func(subj string, resp []byte, e error) {
			response = resp
			err = e
			wg.Done()
		})
	wg.Wait()
	return response, err
}

// Request : synchrony request
// 	subject string eg call.ipwatch.ipv4.start
func (c *RsClient) Request2(subject string, msg interface{}) (response []byte, err error) {

	var wg sync.WaitGroup

	request := Request{Params: msg}

	payload, err := json.Marshal(&request)
	if err != nil {
		return response, err
	}

	// send a request
	wg.Add(1)
	c.SendRequest(subject,
		payload,
		func(subj string, resp []byte, e error) {
			response = resp
			err = e
			wg.Done()
		})
	wg.Wait()
	return response, err
}
