package main

import (
	//"bitbucket.org/orangeparis/ines/heartbeat"
	//"bitbucket.org/orangeparis/ines/nping"
	//"bitbucket.org/orangeparis/ines/publisher"
	"bitbucket.org/orangeparis/ipwatch"
	"context"
	"flag"
	"fmt"
	nats "github.com/nats-io/nats.go"
	"log"
	"os"
	//"time"
)

/*
	runs an ipwatch agent
		name , cidr , port , nats

	publish events

		event.ipwatch.ip4.<name>.found     { Ip: string , Port: string }



*/

var (
	Version = "0.1"

	name    = flag.String("name", "0", "name of the watcher")
	natsURL = flag.String("nats", "nats://127.0.0.1:4222", "nats server")

	cidr = flag.String("cidr", "", "cidr to scan eg 192.168.1.0/24")
	port = flag.Int("port", 80, "port to scan: eg 80 for http")

	helper = `scan network for  cidr/port  and publish messages to nats server when found an open port
sample usage : ipwagent --name local80 --nats nats://127.0.0.1:4222 --cidr "192.168.1.0/24" --port 80
options:
--name string
	name of the sniffer (default "0") (eg 0 , will publish to event.ipwatch.ip4.<name>.found) 
-- cidr string
	the cidr to scan ( eg 192.168.1.0/24)
-- port
	the port to scan ( eg 80 for http server )
--nats string
    	nats server (default "nats://127.0.0.1:4222")

nats messages published: 

	event.ipwatch.ipv4.<name>.found     { Ip: string , Port: string }

	
`
)

func main() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, fmt.Sprintf("ipwagent %s\n", Version))
		fmt.Fprintf(os.Stderr, helper)
	}

	flag.Parse()

	log.Printf("ipwatch ipv4 scanner starts with nats server at : %s\n", *natsURL)
	log.Printf("ipwatch ipv4 scanner search for cidr: %s, port:  %d\n", *cidr, *port)
	subject := "event.ipwatch.ipv4." + *name + ".found"
	log.Printf("ipwatch ipv4 scanner publish messages on subject: [%s]\n", subject)

	// start the ipwatch service
	run(*name, *cidr, *port, *natsURL)

	// wait forever
	select {}

	//log.Printf("ipwatch service: exit")
}

func run(name, cidr string, port int, natsURL string) {

	nc, err := nats.Connect(natsURL)
	if err != nil {
		log.Fatal(err.Error())
		return
	}
	defer nc.Close()
	//var cidr ipwatch.Cidr = "192.168.1.0/24"
	//cidr := "192.168.1.0/24"
	w, _ := ipwatch.NewIpv4Watcher(nc, name, cidr, port, 500, 10)
	//w := ipwatch.Ipv4Watcher{nc, "1", cidr, []int{80}, 500 * time.Millisecond, 0}
	ctx := context.Background()

	// start the watcher
	go w.Run(ctx)

}
