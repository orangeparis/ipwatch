package ipwatch

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	nats "github.com/nats-io/nats.go"
)

/*

	a controller for ipv4watchers

*/

//var controller *Ipv4Watchers

type Ipv4Handler struct {
	Cancel  context.CancelFunc
	Params  *Ipv4WatcherParams
	Watcher *Ipv4Watcher
}

// Ipv4Watchers a Contrller fot ipv4 watcher
type Ipv4Watchers struct {
	mux  sync.Mutex
	Cnx  *nats.Conn
	Name string // resource name eg :ipv4
	//MemberList []string
	Members map[string]Ipv4Handler
}

// NewIpv4Watchers create an ipv4 watcher controller
func NewIpv4Watchers(nc *nats.Conn, resourceNname string) *Ipv4Watchers {
	c := &Ipv4Watchers{Cnx: nc, Name: resourceNname}
	c.Members = make(map[string]Ipv4Handler)
	return c
}

// New create an new ipv4watcher
func (w *Ipv4Watchers) New(name, cidr string, port, timeout, logLevel int) (err error) {

	w.mux.Lock()
	defer w.mux.Unlock()

	watcher, err := NewIpv4Watcher(w.Cnx, name, cidr, port, timeout, logLevel)
	//watcher := &Ipv4Watcher{w.Cnx, params.Name, params.Cidr, params.Ports, tm, logLevel}
	if err != nil {
		return err
	}

	// set the member

	ctx, cancel := context.WithCancel(context.Background())
	params := &Ipv4WatcherParams{watcher.Name, watcher.Cidr, watcher.Ports, watcher.Timeout, watcher.LogLevel}
	h := Ipv4Handler{Cancel: cancel, Params: params, Watcher: watcher}
	w.Members[params.Name] = h

	//w.MemberList = append(w.MemberList, fmt.Sprintf("ipwatch.ipv4.%s", name))

	// start the watcher
	go watcher.Run(ctx)

	return err

}

// Clear an ipv4 watcher
func (w *Ipv4Watchers) Clear(name string) {

	w.mux.Lock()
	defer w.mux.Unlock()

	handler, ok := w.Members[name]
	if ok {
		// cancel the watcher
		handler.Cancel()
		time.Sleep(1 * time.Second)
		delete(w.Members, name)
	}

}

// ClearAll all ipv4 watchers
func (w *Ipv4Watchers) ClearAll() {

	w.mux.Lock()
	defer w.mux.Unlock()

	for name := range w.Members {
		handler, ok := w.Members[name]
		if ok {
			// cancel the watcher
			handler.Cancel()
			delete(w.Members, name)
		}
	}
	time.Sleep(1 * time.Second)

}

// MemberList : list of watcher rid
func (w *Ipv4Watchers) MemberList() (list []string) {
	w.mux.Lock()
	defer w.mux.Unlock()
	for name := range w.Members {
		list = append(list, w.GetRid(name))
	}
	return list
}

// IsMember : check an ipv4watcher exists
func (w *Ipv4Watchers) IsMember(name string) bool {
	w.mux.Lock()
	defer w.mux.Unlock()
	_, ok := w.Members[name]
	return ok
}

// Get : get item
func (w *Ipv4Watchers) Get(name string) *Ipv4Handler {
	item := w.Members[name]
	return &item
}

// GetRid : check an ipv4watcher exists
func (w *Ipv4Watchers) GetRid(name string) string {
	return fmt.Sprintf("ipwatch.ipv4.%s", name)
}

// GetHits : get all hits of an ipv4 watcher
func (w *Ipv4Watchers) GetHits(name string) (hits map[string]int64, err error) {

	// check if there is such a watcher
	if w.IsMember(name) == false {
		err = errors.New("Not found")
		return hits, err
	}
	// get the hits of the watcher
	hits = w.Members[name].Watcher.GetHits()
	return hits, err
}
