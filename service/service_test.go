package service_test

import (
	"testing"
	"time"

	"bitbucket.org/orangeparis/ipwatch/client"
	"bitbucket.org/orangeparis/ipwatch/service"
	//"github.com/stretchr/testify/assert"
	//"fmt"
)

func TestService(t *testing.T) {

	natsURL := "nats://demo.nats.io:4222"

	// start the service
	s := service.NewService(natsURL)
	time.Sleep(2 * time.Second)

	//
	// send messages
	//

	// create a client
	cl, err := client.NewClient(natsURL, 5)
	if err != nil {
		panic(err)
	}
	defer cl.Close()

	// starts an ipv4 watcher
	msg, err := cl.StartIpv4("http", "192.168.1.0/24", 80, 500, 0)
	if err != nil {
		println(err.Error())
		t.Fail()
		return
	} else {
		println(string(msg.Data))
	}

	// list active watchers
	msg, err = cl.ListIpv4()
	if err != nil {
		println(err.Error())
		t.Fail()
		return
	} else {
		println(string(msg.Data))
	}

	// get an active watchers
	msg, err = cl.GetIpv4("http")
	if err != nil {
		println(err.Error())
		t.Fail()
		return
	} else {
		println(string(msg.Data))
	}
	time.Sleep(1 * time.Second)

	// get hits of http watcher
	msg, err = cl.GetIpv4Hits("http")
	if err != nil {
		println(err.Error())
		t.Fail()
		return
	} else {
		println(string(msg.Data))
	}

	// kill all ipv4 watcher
	msg, err = cl.ClearAllIpv4()
	if err != nil {
		println(err.Error())
		t.Fail()
		return
	} else {
		println(string(msg.Data))
	}

	// kill an ipv4 watcher
	msg, err = cl.StopIpv4("http")
	if err != nil {
		println(err.Error())
		t.Fail()
		return
	} else {
		println(string(msg.Data))
	}

	time.Sleep(10 * time.Second)
	// shutdown the service
	_ = s.Shutdown()

}
