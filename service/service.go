package service

import (
	"bitbucket.org/orangeparis/ipwatch"
	"github.com/jirenius/go-res"
	nats "github.com/nats-io/nats.go"
)

//var natsServer = "nats://demo.nats.io:4222"

// a controller to lauchn ipv4 watchers
var ipv4Controller *ipwatch.Ipv4Watchers

// NewService starts the nats service
func NewService(natsURL string) *res.Service {

	// create a ipv4 watcher controller
	nc, err := nats.Connect(natsURL)
	if err != nil {
		panic(err)
	}
	defer nc.Close()
	// create a controller
	ipv4Controller = ipwatch.NewIpv4Watchers(nc, "ipv4")

	// Create a new RES Service
	s := res.NewService("ipwatch")

	// add ipv4 resource
	Ipv4Resource(s)

	// Start the service
	go s.ListenAndServe(natsURL)

	return s
}
