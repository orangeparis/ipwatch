package service

import (
	"fmt"

	"github.com/jirenius/go-res"
)

/*

	resource ipwatch.ipv4

*/

// NewIn message for ipwatch.ipv4.new
type NewIn struct {
	Name    string `json:"name"` // watcher name
	Cidr    string `json:"cidr"` // Cidr
	Port    int    `json:"port"`
	Timeout int    `json:"timeout"` // timeout in seconds
}

// NewOut response message for ipwatch.ipv4.new
type NewOut struct {
	Rid string `json:"rid"` // rid , resource  identier eg ipwatch.ipv4.123
}

// Ipv4Resource :  handle resource ipwatch.ipv4
func Ipv4Resource(s *res.Service) {

	// handle collection ipwatch.ipv4 resource
	s.Handle("ipv4",
		res.Access(res.AccessGranted),

		//  get.ipwatch.ipv4  list all ipv4 watchers
		res.GetCollection(func(r res.CollectionRequest) {
			r.Collection(ipv4Controller.MemberList())
		}),

		//  call.ipwatch.ipv4.start  create an ipv4 watcher  ipwatch.ipv4.$id
		res.Call("start", func(r res.CallRequest) {
			var p = &NewIn{}
			r.ParseParams(p)
			// create an ipv4 watcher
			//cnf := ipwatch.NewIpv4WatchersParams(p.Name, p.Cidr, p.Port, p.Timeout, 90)
			err := ipv4Controller.New(p.Name, p.Cidr, p.Port, p.Timeout, 90)
			if err != nil {
				rerr := &res.Error{Code: "500", Message: err.Error()}
				r.Error(rerr)
			} else {
				msg := &NewOut{Rid: fmt.Sprintf("ipwatch.ipv4.%s", p.Name)}
				//msg := fmt.Sprintf("{\"rid\":\"ipwatch.ipv4.%s\"", p.Name)
				r.OK(msg)
			}
		}),

		//  call.ipwatch.ipv4.clear     delete all watchers
		res.Call("clear", func(r res.CallRequest) {
			ipv4Controller.ClearAll()
			r.OK("")
		}),
	)

	// handle ipwatch.ipv4.$id resources
	s.Handle("ipv4.$id",
		res.Access(res.AccessGranted),

		// get.ipwatch.ipv4.$id  get watcher representation
		res.GetModel(func(r res.ModelRequest) {
			item := ipv4Controller.Get(r.PathParam("id"))
			if item == nil {
				r.NotFound()
			} else {
				r.Model(item.Params)
			}
		}),

		// call.ipwatch.ipv4.$id.stop
		res.Call("stop", func(r res.CallRequest) {
			name := r.PathParam("id")
			ipv4Controller.Clear(name)
			// create an ipfv4 watcher
			r.OK("")
		}),
	)

	// handle ipwatch.ipv4.$id.hits
	s.Handle("ipv4.$id.hits",
		res.Access(res.AccessGranted),
		res.GetModel(func(r res.ModelRequest) {
			item := ipv4Controller.Get(r.PathParam("id"))
			if item == nil {
				r.NotFound()
			} else {
				hits := item.Watcher.GetHits()
				r.Model(hits)
			}
		}),
	)

}
