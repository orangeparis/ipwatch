package ipwatch

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"sync"
	"time"

	nats "github.com/nats-io/nats.go"
)

/*

	an ipwatcher agent

	publish events

	subject: event.ipwatch.ipv4.$id.found
	message { ip:string, port:int }



*/

// FoundMsg format of message sent on event.ipwatch.ipv4.$id.found
type FoundMsg struct {
	IP   string `json:"ip"`
	Port int    `json:"port"`
}

type Ipv4WatcherParams struct {
	Name     string
	Cidr     Cidr
	Ports    []int
	Timeout  time.Duration // time to wait for tcp dialIn
	LogLevel int
}

func NewIpv4WatchersParams(name string, cidr string, port int, timeout int, logLevel int) *Ipv4WatcherParams {

	w := &Ipv4WatcherParams{
		Name:     name,
		LogLevel: logLevel,
	}
	w.Cidr = Cidr(cidr)
	w.Ports = append(w.Ports, port)
	w.Timeout = time.Duration(timeout) * time.Millisecond

	return w
}

// Ipv4Watcher  an ipv4 watchertimeout
type Ipv4Watcher struct {
	*nats.Conn
	Name     string
	Cidr     Cidr
	Ports    []int
	Timeout  time.Duration // time to wait for tcp dialIn
	LogLevel int

	mux  sync.Mutex
	Hits map[string]int64 // map {  "192.168.1.1:23": timestamp , ... }

}

// NewIpv4Watcher : create a watcher
func NewIpv4Watcher(cnx *nats.Conn, name string, cidr string, port int, timeout int, logLevel int) (w *Ipv4Watcher, err error) {

	if name == "" {
		name = "noname"
	}
	w = &Ipv4Watcher{
		Conn:     cnx,
		Name:     name,
		LogLevel: logLevel,
		Hits:     make(map[string]int64),
	}
	w.Cidr = Cidr(cidr)
	if err != nil {
		return nil, err
	}
	w.Ports = append(w.Ports, port)
	w.Timeout = time.Duration(timeout) * time.Millisecond

	err = w.Cidr.Check()

	return w, err
}

// Run scan the ports of the cidr
func (w *Ipv4Watcher) Run(ctx context.Context) {
	//
	log.Printf("ipwatcher: watcher [%s] starts\n", w.Name)
	addrs := w.Cidr.RangeNetAddr(w.Ports)
	for {
		start := time.Now()
		for tcpAddr := range addrs {
			// check network
			status, took := CheckTCPPort(tcpAddr, w.Timeout, w.LogLevel)
			if status == true {
				_ = took
				// the port is open : send an event
				w.AddHit(tcpAddr)
				response := &FoundMsg{tcpAddr.IP.String(), tcpAddr.Port}
				w.SendEvent("found", response)
				// subject := fmt.Sprintf("event.ipwatch.ipv4.%s.found", w.Name)
				// response := &FoundMsg{tcpAddr.IP.String(), tcpAddr.Port}
				// msg, _ := json.Marshal(response)
				// w.Publish(subject, msg)
				// if w.LogLevel >= 10 {
				// 	log.Printf("ipwatcher: emit message on %s the message:\n%s\n", subject, string(msg))
				// }
			} else {
				// port is NOT open
				// maybe send a change event if port was open before ?
			}
			// asked for shutdown ?
			select {
			case <-ctx.Done():
				log.Printf("ipwatcher: watcher [%s] exits\n", w.Name)
				return
			default:
				// continue
			}
		}
		elapsed := time.Since(start)
		// send an alapsed  message event.ipwatch.ipv4.<name>.elapsed
		_ = elapsed
	}
}

// AddHit : add an open event on tcpaddr to hits
func (w *Ipv4Watcher) AddHit(tcpAddr net.TCPAddr) {

	addr := tcpAddr.String()
	timestamp := time.Now().Unix()

	w.mux.Lock()
	defer w.mux.Unlock()

	_, ok := w.Hits[addr]
	if ok {
		// this is a new Entry: send a change event ?
		subject := fmt.Sprintf("event.ipwatch.ipv4.%s.hits.change", w.Name)
		payload := make(map[string]int64)
		payload[addr] = timestamp
		//  "values": {"192.168.1.1:23", 1571156107}
		_ = subject
		_ = payload

	}
	w.Hits[addr] = timestamp

}

// GetHits return the hits
func (w *Ipv4Watcher) GetHits() map[string]int64 {
	w.mux.Lock()
	defer w.mux.Unlock()
	return w.Hits
}

// SendEvent : send event of the form event.ipwatch.ipv4.$name.$event
func (w *Ipv4Watcher) SendEvent(eventName string, response interface{}) {

	subject := fmt.Sprintf("event.ipwatch.ipv4.%s.%s", w.Name, eventName)
	msg, _ := json.Marshal(response)
	w.Publish(subject, msg)
	if w.LogLevel >= 10 {
		log.Printf("ipwatcher: emit on [%s] the message:\n%s\n", subject, string(msg))
	}
	return
}

//
//	basic tcp helpers
//

// CheckTCPPort : check open status on a tcp port
// return true if open
func CheckTCPPort(tcpAddr net.TCPAddr, timeout time.Duration, logLevel int) (status bool, elapsed time.Duration) {
	//
	// try to open port , if OK return true
	//
	start := time.Now()
	conn, err := net.DialTimeout("tcp", tcpAddr.String(), timeout)
	elapsed = time.Since(start)

	//defer conn.Close()
	if err == nil {
		// port is open
		if logLevel >= 10 {
			log.Printf("ipwatcher: -- checkaddr: CONNECTION OK  to %s\n", tcpAddr.String())
		}
		conn.Close()
		status = true

	} else {
		// cannot open port
		if logLevel >= 90 {
			log.Printf("ipwatcher: -- fail to connect to %s\n", tcpAddr.String())
		}
	}
	return status, elapsed
}
